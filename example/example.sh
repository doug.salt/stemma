#!/bin/sh

pdftotext "Grant_Agreement-763912-SMARTEES.pdf"
WNHOME=. ../bin/get_frequencies.pl \
    --output results.csv \
    "Grant_Agreement-763912-SMARTEES.txt"
../bin/plot_frequency_vs_familiarity.R results.csv
rm results.csv
rm Grant_Agreement-763912-SMARTEES.txt
