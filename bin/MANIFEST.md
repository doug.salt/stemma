+ abbreviation.pl - a silly tool that takes some text as input, and tries to
  produce a list of unique abbreviations. The key is the regex.
+ columnate.pl - a small program to neatly columnate stuff for presentation.
+ detect_latest_version.pl - a program to walk  a directory and pick out the
  latest and greatest version of a document.
+ get_frequencies.pl - the meat and potatoes; the real deal, the raison d'être,
  the-all and end-all - the thing that does the bizz of analysing documentation
  and getting word-counts.
+ getowl.sh - a script to remotely obtain schema's hidden within document
  specifications
+ MANIFEST.md - this file
+ plot_frequency_vs_familiarity.R
+ same.pl - an experiment to find similar file names.
+ types.pl - a simple program to list the different types of file types (by
  suffix) in a particular directory.
+ wordnet.pl - small test program to query WordNet and make sure everything is
  working.
