#!/usr/bin/perl

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

use strict;
use warnings;
use POSIX;
use Carp;
use Getopt::Long;
use File::Spec;

# I did use WordNet::QueryData to get the forms, but the forms are always n, v,
# a and r, so we just looop over them.

use Lingua::Wordnet;
use Lingua::EN::Inflexion qw(noun);

my $IGNORE_WORDS_BELOW_THIS_SIZE = 4;
my @uname = uname();

my $WNHOME;
if ($uname[0] eq "Linux") {
    $WNHOME="/home/doug/git/stemma/example/";
}
else {
    $WNHOME="c:/users/ds42723/git/stemma/example/wordnet/";
}

our $version = 0.1;

=pod
=head1 NAME

get_frequencies.pl - count the instances of a word and calculate our version of
the familiarity of that word.

=head1 SYNOPSIS

get_frequencies.pl 
    -cgirxXyYpPowHDV 
    --cutoff
    --graph
    --help 
    --inclination
    --output 
    --result
    --verbose 
    --version 
    --x-minimum
    --x-maximum
    --x-percentile
    --y-minimum
    --y-maximum
    --y-percentile
    --word-group-size 
    file_name

=head1 DESCRIPTION

The progam will translate a file into txt and count the frequency of single
words, or word groups from a single word to word groups whose maximum size is
specified by the --word-group-size parameter.

It will then (if the graph parameter is specified) produce a graph showing a
selection of the words of familiarity plotted against log frequency, which
markers showing the percentile limits specified in the parameters. The words
ending in the top-left quadrant of this graph, are, potentially terminology,
and these words are recorded to a file, if a file has been so specified.

=over

=item --output= | -o

The file to which the results of this anlaysis are stored. This will be stored
in the CSV format.  This file may contain initial values, from other scans of
documents, so be aware that this will overwrite existing data. This has the
format:

word or word group,frequency integer value,origin path[;next origin path]...

If the initial values have been provided then this file will be read in and the
results from this scan will be added to those of the initial values. Positions
3, will containing a semi-colon separated list of file names in which the term
originated.

=item --cutoff | -c 

This is the number of occurrences of an n-gram over the total number of words
in a document under which a word will not be display in the resultant graph

=item --verbose | -D

Print out stuff for the purposes of recording what we have done in
terms of processing.

=item --graph | -g 

This is where the file to which the graph should be written. If no graph path
is specified then no graph is written.

=item --filter | -F

This filters obvious nonsense words, such as word groups starting with words
like "is" or ending with "of", etc. 

The default for this is true.

=item --help | -H

Gets this message.

=item --inclination | -i

This is how much the words in the graph will tilt. Set in degrees. The default
is 5 and this must be a float.

=item --result | -r

A pathname for the filtered results, set by --x-percentile and --y-percentile. If the
=item --version | -V

Reports the version number.

=item --word-group-size | -w

This is the word cluster groups you wish to count. This value cannot be less
than 3 (trigram), and can be as high as you like, as long as you have
the computing power. This must be an integer.

=item --x-minimum | -x

The minimum for the X axis for the graph. The default is 0.0. This must be a
float.

=item --x-maximum | -X

The maximum for the X axis for the graph. The default is 10.0. This must be a
float.

=item --x-percentile | -p

This is the bottom percentage that vocabulary must belong to. That is the lower
familiarity, the more like it is to be an unusual word and therefore vocabulary
for the  project.  The default is 80. This must be a integer.

=item --y-minimum | -y

The maximum for the Y axis for the graph. The default is 1.0. This must be a
float.

=item --y-maximum | -Y

The minimum for the X axis for the graph. The default is 8.0 This must be a
float.

=item --y-percentile | -P

This represents the limit at which words are considered to vocabulary if they
are in the top percentage as defined by this value.  The default is 2.  This
must be a integer.

=back

=cut

my ($cutoff, 
    $help, 
    $report_version, 
    $verbose, 
    $output, 
    $filter, 
    $graph,
    $inclination,
    $result,
    $x_minimum,
    $x_maximum,
    $x_percentile,
    $y_minimum,
    $y_maximum,
    $y_percentile,
    $max_group_size 

);

my $usage = "Usage: $0 -ocwFHDV " .
    " --filter" .
    " --graph" .
    " --inclination" .
    " --help " .
    " --output " .
    " --result " .
    " --verbose " .
    " --version " .
    " --x-minimum" .
    " --x-maximum" .
    " --x-percentile" .
    " --y-minimum" .
    " --y-maximum" .
    " --y-percentile" .
    " --word-group-size" .
    " file_name";

croak $usage unless
    GetOptions("output|o:s" => \$output,
               "cutoff|c:s" => \$cutoff,
               "filter|F" => \$filter,
               "graph|g:s" => \$graph,
               "help|H" => \$help,
               "inclination|i:s" => \$inclination,
               "result|r:s" => \$result,
               "verbose|D" => \$verbose,
               "version|V" => \$report_version,
               "x-minimum:s" => \$x_minimum,
               "x-maximum:s" => \$x_maximum,
               "x-percentile:s" => \$x_percentile,
               "x-minimum:s" => \$y_minimum,
               "y-maximum:s" => \$y_maximum,
               "y-percentile:s" => \$y_percentile,
               "word-group-size|w:s" => \$max_group_size);

if ($help) {
    system "perldoc", "$0";
    exit 0;
}

if ($report_version) {
    print STDERR "Version: $version\n";
    exit 0;
}

# Extra validation on the parameters.

if (defined $max_group_size) {
        croak "Non numeric word groups argument"
                if $max_group_size !~ /[0-9]+/;
        croak "Word groups must be at least one (for single words)"
                if $max_group_size < 1;
}
else {
        $max_group_size = 3
}
croak "No input file supplied" unless @ARGV > 0;
croak "Only one input file allowed (for now)" unless @ARGV == 1;

my $path = $ARGV[0];

croak "File $path does not exist"
    unless -f $path;

$path = File::Spec->rel2abs( $path );

$filter = 1 unless defined $filter;
$cutoff = "5.0" unless defined $cutoff;
$graph = '/dev/null' unless defined $graph;
$result = '/dev/null' unless defined $result;
$inclination = "5.0" unless defined $inclination;
$x_minimum = "0.0" unless defined $x_minimum;
$x_maximum = "8.0" unless defined $x_maximum;
$x_percentile = "80" unless defined $x_percentile;
$y_minimum = "1.0" unless defined $y_minimum;
$y_maximum = "8.0" unless defined $y_maximum;
$y_percentile = 2 unless defined $y_percentile;


croak "Invalid frequency cutoff: $cutoff - needs to be a decimal value in ]1,0["
    unless $cutoff =~ /^[0-9]+\.[0-9]+/ && $cutoff >= 0;
    

my $lwn = Lingua::Wordnet->new($WNHOME);

my %frequency;
my %locations;
my %familiarity;

#if (defined $output && -f "$output") {
#    open(INITIAL, "<" . $output)
#        or croak "Unable to open $output: $!";
#    while (<INITIAL>) {
#        my ($word,$frequency,$familiarity,$locations) = split(/\,/,$_);
#        chomp($frequency);
#	chomp($familiarity);
#        $frequency{$word} = $frequency;
#	$familiarity{$word} = $familiarity;
#        if (defined $locations) {
#            chomp($locations);
#            $locations{lc($word)} = $locations;
#        }
#    }
#    close INITIAL;
#}

if ($verbose) {
    if (keys %frequency == 0) {
        print STDERR "No initial values have been provided.\n";
    }
    else {
        print STDERR "Initial values have been provided containing " . (keys %frequency) . " different words.\n"
    }
}

my $new_words = 0;
my $hits = 0;

print STDERR "Processing $path\n" if $verbose;

croak "Document file $path does not exist"
	unless -f $path;

my $filetype = `file --mime-type "$path"`;

if ($filetype =~ /text\/plain/) {
	print STDERR "Dealing with plain text\n" if $verbose;
	open(DOC, "cat $path |")
		or croak "Unable to open the document: $!";
} 
elsif ($filetype =~ /application\/vnd.openxmlformats-officedocument.wordprocessingml.document/) {
	print STDERR "Dealing with Microsoft docx\n" if $verbose;
	open(DOC, "unzip -p \"$path\" | grep --text '<w:r' | sed 's/<w:p[^<\/]*>/ /g' | sed 's/<[^<]*>//g' | grep -v '^[[:space:]]*$' | sed G |")
		or croak "Unable to unzip the document: $!";
}
elsif ($filetype =~ /application\/msword/) {
	print STDERR "Dealing with plain Microsoft doc\n" if $verbose;
	open(DOC, "catdoc \"$path\" |")
		or croak "Unable to catdoc the document: $!";
}
elsif ($filetype =~ /application\/pdf/) {
	print STDERR "Dealing with pdf\n" if $verbose;
	open(DOC, "pdftotext -enc ASCII7 \"$path\" - |")
		or croak "Unable to pdftotext the document: $!";
}
else {
	croak "$path is an unrecognised document type: $filetype";
}

my @words;
my $hyphenated_word;
while (my $line = <DOC>) {
    $line =~ s/[\000-\037]//g;
    next if $line =~ /^$/;
    # Let's split this bugger up into words. I was a bit worried that the below
    # was not adequate tokenization, but on reflection I am convinced this is
    # sufficient. The only thing I did not take care of was hyphenation, and
    # obviously the semantics of all this are purely English.
    my @line_of_words = split(/(\s+|\/|\\)/,$line);
    if (defined $hyphenated_word) {
	if ($lwn->grep($line_of_words[0] . $hyphenated_word) > 0) {
		# The word definitely exists
        	$line_of_words[0] = $hyphenated_word . $line_of_words[0]; 
	}
        $hyphenated_word = undef;
    }
    if ($line_of_words[-1] =~ /\-$/) {
	$hyphenated_word = pop @line_of_words;
	$hyphenated_word =~ s/\-$//;
    }	
    for my $word (@line_of_words) {
	# Filter out some of the crap.
	$word =~ s/\W+$//;
	$word =~ s/'s$//;
	$word =~ s/^\W//;
	next if $word =~ /^\s*$/;
	if ($word !~ /^[A-Za-z\-]+$/) {
		print STDERR "Rejecting non alphanumerics: $word\n" if $verbose; 
		$word = "###NON-WORD-TOKEN###"; 
	}
        elsif ($word =~ /^[A-Z\-]+$/ && length $word < $IGNORE_WORDS_BELOW_THIS_SIZE) {
        	# If this is all uppercase then it is probably an abbreviation.
		print STDERR "Rejecting abbreviation: $word\n" if $verbose; 
		$word = "###NON-WORD-TOKEN###"; 
	}
	elsif ($word =~ qr/(.)\1\1+/) {
		# If there is a repeated set of letters or more than 3
		print STDERR "Rejecting repeated letters: $word\n" if $verbose; 
		$word = "###NON-WORD-TOKEN###"; 
	}
	else {
		$word = lc($word);
	}
    	push @words, $word;
    }
}

# Let's deal with hyphenated words that only exist at the end
close(DOC);
print STDERR "$path contains " . @words . " words.\n"
	if $verbose;

for (my $size = 1; $size <= $max_group_size; $size++) {
    print STDERR "Processing word groups of size $size\n"
	if $verbose;
    WORD: for (my $i = 0; $i <= @words - $size;$i++) {
	my $n_gram = "";
        my @n_gram;
	for (my $j = 0; $j < $size && $words[$i + $j] ne "###NON-WORD-TOKEN###"; $j++ ) {
		#for (my $j = 0; $j < $size ; $j++ ) {
		if ($j > 0) {
			$n_gram .= " " . $words[$i + $j];
		}
		else {
			$n_gram .= $words[$i + $j];
		}
		push @n_gram, $words[$i + $j];
	}
	next WORD if @n_gram < $size - 1
		|| $n_gram eq "";

    # Now deal with plurals. We only want modify the n-gram, if it is a single
    # word, as the "depluralising" may invalidate the sense of an n-gram of 2
    # or more.
	
	if ($size == 1) {
		next WORD if length($n_gram) < $IGNORE_WORDS_BELOW_THIS_SIZE;
		my $singular = noun($n_gram)->singular;
		print STDERR "Changed $n_gram to $singular \n"
		     if $verbose and $n_gram ne $singular;
        $n_gram = $singular;
        $n_gram[0] = $singular
	}

	unless (defined $frequency{$n_gram})  {
	    $new_words ++;
	    $frequency{$n_gram} = 0;
	    # Get the familiarity from WordNet
        my @familiarities;
	    for my $atom (@n_gram) {
	        my $familiarity_total = 0;
            for my $pos (qw (n v a r)) {
		        my $singular = noun($atom)->singular;
		        print STDERR "Changed $atom to $singular \n"
			        if $verbose and $atom ne $singular;
                $atom = $singular;
                my $familiarity = $lwn->familiarity($atom,$pos);
                if (defined $familiarity) {
                    $familiarity_total += $familiarity;
                }
            }
            push @familiarities, $familiarity_total;
        }
        $familiarity{$n_gram} = &minimum(@familiarities);
        print STDERR $n_gram . " has familiarity " . $familiarity{$n_gram} . "\n"
           if $verbose;
    }
	$frequency{$n_gram} ++ ;
	$hits ++;
	# Tag this with the document path name

	if (!defined($locations{$n_gram})){
		$locations{$n_gram} = $path;
	}
	elsif (index($locations{$n_gram},$path) == -1) {
		$locations{$n_gram} .= ";" . $path;
	}

    }
}

my $OUTPUT;
if (defined($output)) {
    open($OUTPUT, ">" . $output) or
        croak "Unable to open $output: $!";
}
else {
    $OUTPUT = *STDOUT;
}

my $total = 0;
for (sort keys %frequency) {
    $total ++ if $_ !~ /\s/;
}

for (sort keys %frequency) {
    my $ngram = $_;
    if ($filter) {
        next if
            $ngram =~ /^about\s+/ || $ngram =~ /\s+about\s+/ || $ngram =~ /\s+about$/ ||
            $ngram =~ /^abm\s+/ || $ngram =~ /\s+abm\s+/ || $ngram =~ /\s+abm$/ ||
            $ngram =~ /^ac\s+/ || $ngram =~ /\s+ac\s+/ || $ngram =~ /\s+ac$/ ||
            $ngram =~ /^also\s+/ || $ngram =~ /\s+also\s+/ || $ngram =~ /\s+also$/ ||
            $ngram =~ /^at\s+/ || $ngram =~ /\s+at\s+/ || $ngram =~ /\s+at$/ ||
            $ngram =~ /^as\s+/ || $ngram =~ /\s+as\s+/ || $ngram =~ /\s+as$/ ||
            $ngram =~ /^are\s+/ || $ngram =~ /\s+are\s+/ || $ngram =~ /\s+are$/ ||
            $ngram =~ /^an\s+/ || $ngram =~ /\s+an\s+/ || $ngram =~ /\s+an$/ ||
            $ngram =~ /^be\s+/ || $ngram =~ /\s+be\s+/ || $ngram =~ /\s+be$/ ||
            $ngram =~ /^but\s+/ || $ngram =~ /\s+but\s+/ || $ngram =~ /\s+but$/ ||
            $ngram =~ /^by\s+/ || $ngram =~ /\s+by\s+/ || $ngram =~ /\s+by$/ ||
            $ngram =~ /^can\s+/ || $ngram =~ /\s+can\s+/ || $ngram =~ /\s+can$/ ||
            $ngram =~ /^dc\s+/ || $ngram =~ /\s+dc\s+/ || $ngram =~ /\s+dc$/ ||
            $ngram =~ /^de\s+/ || $ngram =~ /\s+de\s+/ || $ngram =~ /\s+de$/ ||
            $ngram =~ /^in\s+/ || $ngram =~ /\s+in\s+/ || $ngram =~ /\s+in$/ ||
            $ngram =~ /^it\s+/ || $ngram =~ /\s+it\s+/ || $ngram =~ /\s+it$/ ||
            $ngram =~ /^ii\s+/ || $ngram =~ /\s+ii\s+/ || $ngram =~ /\s+ii$/ ||
            $ngram =~ /^iii\s+/ || $ngram =~ /\s+iii\s+/ || $ngram =~ /\s+iii$/ ||
            $ngram =~ /^iv\s+/ || $ngram =~ /\s+iv\s+/ || $ngram =~ /\s+iv$/ ||
            $ngram =~ /^vi\s+/ || $ngram =~ /\s+vi\s+/ || $ngram =~ /\s+vi$/ ||
            $ngram =~ /^not\s+/ || $ngram =~ /\s+not\s+/ || $ngram =~ /\s+not$/ ||
            $ngram =~ /^on\s+/ || $ngram =~ /\s+on\s+/ || $ngram =~ /\s+on$/ ||
            $ngram =~ /^ro\s+/ || $ngram =~ /\s+ro\s+/ || $ngram =~ /\s+ro$/ ||
            $ngram =~ /^so\s+/ || $ngram =~ /\s+so\s+/ || $ngram =~ /\s+so$/ ||
            $ngram =~ /^there\s+/ || $ngram =~ /\s+there\s+/ || $ngram =~ /\s+there$/ ||
            $ngram =~ /^will\s+/ || $ngram =~ /\s+will\s+/ || $ngram =~ /\s+will$/ ||
            $ngram =~ /^yes\s+/ || $ngram =~ /\s+yes\s+/ || $ngram =~ /\s+yes$/ ||
            $ngram =~ /^of\s+/ || $ngram =~ /\s+of$/ ||
            $ngram =~ /^a\s+/ || $ngram =~ /\s+a$/ ||
            $ngram =~ /^and\s+/ || $ngram =~ /\s+and$/ ||
            $ngram =~ /^become\s+/ || $ngram =~ /\s+become$/ ||
            $ngram =~ /^between\s+/ || $ngram =~ /\s+between$/ ||
            $ngram =~ /^have\s+/ || $ngram =~ /\s+have$/ ||
            $ngram =~ /^is\s+/ || $ngram =~ /\s+is$/ ||
            $ngram =~ /^more\s+/ || $ngram =~ /\s+more$/ ||
            $ngram =~ /^the\s+/ || $ngram =~ /\s+the$/ ||
            $ngram =~ /^we\s+/ || $ngram =~ /\s+we$/ ||
            $ngram =~ /^when\s+/ || $ngram =~ /\s+when$/ ||
            $ngram =~ /^which\s+/ || $ngram =~ /\s+which$/ ||
            $ngram =~ /^who\s+/ || $ngram =~ /\s+who$/ ||
            $ngram =~ /^van\s+/ || $ngram =~ /\s+van$/ ||
            $ngram =~ /\s+making$/ ||
            $ngram =~ /\s+much$/ ||
            $ngram =~ /\s+different$/ ||
            $ngram =~ /\s+no$/ ||
            $ngram =~ /\s+three$/ ||
            $ngram =~ /\s+almost$/ ||
            $ngram =~ /\s+do$/ ||
            $ngram =~ /\s+other$/ ||
            $ngram =~ /\s+each$/ ||
            $ngram =~ /\s+might$/ ||
            $ngram =~ /\s+may$/ ||
            $ngram =~ /\s+within$/ ||
            $ngram =~ /^ho ur/;
    }

    print $OUTPUT "$ngram,$frequency{$ngram},$familiarity{$ngram}";
    if (defined($locations{$ngram})) {
        print $OUTPUT "," . $locations{$ngram};
    }
    print $OUTPUT "\n";
}

open (my $R, "|R --vanilla --slave --quiet") ||
    die "Unable to open R code";

$R->print("in.results <- \"" . $output . "\"\n");
$R->print("graph <- \"" . $graph . "\"\n");
$R->print("out.csv <- \"" . $result . "\"\n");

$R->print("x.maximum <- " . $x_maximum . "\n");
$R->print("x.minimum <- " . $x_minimum . "\n");
$R->print("x.percentile <- " . $x_percentile . "\n");

$R->print("y.maximum <- " . $y_maximum . "\n");
$R->print("y.minimum <- " . $y_minimum . "\n");
$R->print("y.percentile <- " . (100 - $y_percentile) . "\n");

$R->print("inclination <- " . $inclination . "\n");

my $r_script = << 'RSCRIPT';

raw <- read.csv(in.results)
colnames(raw) <- c("ngram","frequency","familiarity","locations")
processed <- subset(raw,frequency >= 8.0,)

pdf(graph)


plot(x=jitter(processed$familiarity,factor=2,amount=2),
        y=jitter(processed$frequency,factor=2,amount=2),
        xlab="Familiarity",
        ylab="Frequency",
        ylim=c(y.minimum,y.maximum),
        xlim=c(x.minimum,x.maximum),
        type="n",
        ylog=TRUE)
text(processed$familiarity, log(processed$frequency), srt=inclination, processed$ngram, cex=0.5, col = rgb(runif(5),runif(5),runif(5)))

quantile.frequency = quantile(raw$frequency, prob = c((as.double(y.percentile) / 100)))
cat ("Percentile frequency of ", y.percentile, " gives a value of ", quantile.frequency, "\n")
abline(h = log(quantile.frequency), col = 'blue')
text(2 * (x.maximum - x.minimum) / 3, log(quantile.frequency) - 0.2, paste("percentile frequency = ", y.percentile),  col="blue")

quantile.familiarity = quantile(raw$familiarity, prob = c(as.double(x.percentile) / 100))
cat ("Percentile familiarity of ", x.percentile, " gives a value of ", quantile.familiarity, "\n")
text(quantile.familiarity, y.maximum - 1, paste("percentile\nfamiliarity\n", x.percentile), col="red")
abline(v = quantile.familiarity, col = 'red')

out <- subset(raw, familiarity <= quantile.familiarity & frequency >= quantile.frequency)
write.csv(out, file=out.csv, row.names = F) 

RSCRIPT

$R->print($r_script);
$R->close;

print "Total number of different words = $total\n";
close $OUTPUT if defined $output;

sub median {
    return 0 unless @_ > 0;
    my @sorted = sort @_;
    my $freq = @sorted;
    if ($freq % 2 == 1 ) {
        return $sorted[floor($freq / 2) - 1];
    } else {
        return ($sorted[$freq / 2 - 1] + $sorted[$freq / 2]) / 2;
    }
}

sub minimum {
    my $result = $_[0];
    for (@_) {
        $result = $_ if $_ < $result;
    }
    $result;
}
print STDERR "Have added $new_words new words to $output\n"
	if $verbose;

__END__

=head1 FILES

L<Lingua::Wordnet|Lingua::Wordnet> uses the WordNet databases
converted in Berkley DB database binary format which are a lot faster. However
to get the WordNet files into this format then you must convert them.

The usual method of installing the module will work: 

C<perl -MCPAN -e 'install Lingua::Wordnet'>

However this does not install the conversion script, so you have to down the
actual tar.gz from cpan.org and extract the files. In the package is the
convertdb.pl, which you must then run:

C<perl convertdb.pl /usr/lib/wnres/dict/>

Say, for instance if you were running this in cygwin.

The converted WordNet files (see above). This are currently in
F<~/git/SMARTEES/wordnet>.

=head1 AUTHOR

Written by Doug Salt.

=head1 REPORTING BUGS

Please send all bug reports to info@hutton.ac.uk.

=head1 COPYRIGHT

Copyright  ©  2020  The James Hutton Institute.  License GPLv3+: GNU
GPL version 3 or later L<http://gnu.org/licenses/gpl.html>.
This is free software: you are free  to  change  and  redistribute  it.
There is NO WARRANTY, to the extent permitted by law.

=head1 SEE ALSO

