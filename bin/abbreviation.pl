#!/usr/bin/perl

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# April 2019

use strict;
use warnings;

=encoding UTF-8

=head1 NAME

abbreviation.pl - a silly tool that takes some text as input, and tries to produce a list of
unique abbreviations. The key is the regex.

=head1 SYNOPSIS

B<abbreviations> C<some-text-file>

=cut

my %set;

while (my $string = <>) {
    chomp;
    while ($string =~ /(README|
        MANIFEST|
        (:[[:upper:]]([[:upper:]])+:)|
        [\_\<]*([[:upper:]]([[:upper:]])+[\_>]*)|
        ([[:upper:]]\s([[:upper:]]\s)+)|
        ([[:upper:]]\.\s([[:upper:]]\.\s)+)|
        ([[:upper:]]\.([[:upper:]]\.)+))/gx ) {
        my $abbrev = $1;
        next if $abbrev =~ /:.+:/;
        next if $abbrev =~ /README/;
        next if $abbrev =~ /MANIFEST/;
        next if $abbrev =~ /^(\_|\<)+/;
        next if $abbrev =~ /(\_|\>)+$/;
        chomp $abbrev;
        $set{$abbrev}=$abbrev;
    }
}
for ( sort keys %set) {
    print "$_\n";
}

=head1 AUTHOR

Written by Doug Salt.

=head1 REPORTING BUGS

Please send all bug reports to info@hutton.ac.uk.

=head1 COPYRIGHT

Copyright  ©  2020  The James Hutton Institute.  License GPLv3+: GNU
GPL version 3 or later L<http://gnu.org/licenses/gpl.html>.
This is free software: you are free  to  change  and  redistribute  it.
There is NO WARRANTY, to the extent permitted by law.

=head1 SEE ALSO

