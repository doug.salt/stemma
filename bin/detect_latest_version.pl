#!/usr/bin/env perl

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# April 2020

=encoding UTF-8

=head1 NAME 

detect_latest_version - A perl script to walk a directory structure and
determined which is the latest version of a document.

=head1 SYNOPSIS

C<detect_latest_version.pl directory>

=head1 DESCRIPTION

=cut

print "some code"


=pod

=head1 AUTHOR

Written by Doug Salt.

=head1 REPORTING BUGS

Please send all bug reports to info@hutton.ac.uk.

=head1 COPYRIGHT

Copyright © 2020  The James Hutton Institute.  License GPLv3+: GNU
GPL version 3 or later L<http://gnu.org/licenses/gpl.html>.
This is free software: you are free  to  change  and  redistribute  it.
There is NO WARRANTY, to the extent permitted by law.

=head1 SEE ALSO


