#!/usr/bin/perl

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# Date: March 2018

use strict;
use warnings;
use Carp;
use POSIX;

use Lingua::Wordnet;
use WordNet::QueryData;

our $version = "0.1";

my $WNHOME;
my $LinguaHOME;

my @uname = uname();

if ($uname[0] eq "Linux") {
    $WNHOME="/home/ds42723/git/SMARTEES/wordnet/";
    $LinguaHOME= "/usr/dict/";
}
else {
    $WNHOME="c:/users/ds42723/git/SMARTEES/wordnet/";
    $LinguaHOME= "/usr/lib/wnres/dict/";
}

my $wn = WordNet::QueryData->new("$LinguaHOME");
my $lwn = Lingua::Wordnet->new($WNHOME);

=encoding UTF-8

=head1 NAME

wordnet.pl - Use the perl libraries for manipulating WordNet

=head1 SYNOPSIS

wordnet.pl some_word

=head1 DESCRIPTION

A small test program to run and play around with the
L<Lingua::Wordnet|Lingua::Wordnet> and
L<WordNet::QueryData|WordNet::QueryData>.  It takes a single word with the part
of speech it is in (n - noun, v - verb and a - adjective) and reports
everything that is known about that word.

This was taken from the old version used to run on GLAMURS. It is using two
database and I don't like that, so hopefully once complete this version will do
the same just using the L<Lingua::Wordnet|Lingua::Wordnet> package.

The reason there are two database is that
L<WordNet::QueryData|WordNet::QueryData> uses the original text files which are
quite slow. L<Lingua::Wordnet|Lingua::Wordnet> uses the WordNet databases
converted in Berkley DB database binary format which are a lot faster. However
to get the WordNet files into this format then you must convert them.

The usual method of installing the module will work: 

C<perl -MCPAN -e 'install Lingua::Wordnet'>

However this does not install the conversion script, so you have to down the
actual tar.gz from cpan.org and extract the files. In the package is the
convertdb.pl, which you must then run:

C<perl convertdb.pl /usr/lib/wnres/dict/>

Say, for instance if you were running this in cygwin.

=cut


#       We need to find which Part of Speech (POS) the word may be used in.
#       To do this we use the stand WordNet Perl interface. Since a word
#       may occur in 1 or more POS then we will take the average of the
#       familiarity over the how many POS the word can appear in.
#       POS may take the values
#            v - verb
#            n - noun
#            a - adjective
#            r - adverb

my $word = $ARGV[0];
my $pos = $ARGV[1];
$pos="n" unless defined $pos;
my $word_familiarity = $lwn->familiarity($word,$pos);
print "Familiarity = $word_familiarity\n"
	if defined $word_familiarity;
my @syn = $lwn->lookup_synset($word,$pos);
for (@syn) { print "Syn: $_\n"; }
#print "morph: " . $lwn->morph($word, $pos) . "\n";
for ($wn->listAllWords($word)) {
	print "listAllWords: $_\n";
}
for ($wn->queryWord($word)) {
	print "queryWord: $_\n";
}
for ($wn->queryWord($word, "also")) {
	print "queryWord:also: $_\n";
}
for ($wn->queryWord($word, "deri")) {
	print "queryWord:deri: $_\n";
}
for ($wn->querySense($word)) {
	print "querySense: $_\n";
}

foreach (qw(n v a r)) {
	if (my $fam = $lwn->familiarity($word, $_)) {
		print qq|\tLingua::Wordnet familiarity in '$_': $fam\n|;
	}
	
}

print "Found " . $lwn->grep($word) . "\n";
for ($lwn->grep($word)) {
	print "grepped: $_\n"
}

__END__

=head1 FILES

The WordNet native files. On Cygwin this is F</usr/lib/wnres/dict/>. This is
used by L<WordNet::QueryData|WordNet::QueryData>.

The converted WordNet files (see above). This are currently in
F<~/git/SMARTEES/wordnet>.

=head1 AUTHOR

Written by Doug Salt.

=head1 REPORTING BUGS

Please send all bug reports to info@hutton.ac.uk.

=head1 COPYRIGHT

Copyright  ©  2020  The James Hutton Institute.  License GPLv3+: GNU
GPL version 3 or later L<http://gnu.org/licenses/gpl.html>.
This is free software: you are free  to  change  and  redistribute  it.
There is NO WARRANTY, to the extent permitted by law.

=head1 SEE ALSO

