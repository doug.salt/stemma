#!/usr/bin/perl

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# March 2019

use strict;
use warnings;
use Text::Fuzzy;
use File::Basename;

=encoding UTF-8

=head1 NAME

same.pl - an experiment to see if we can detect similar file names using the L<Text::Fuzzy> package.
See L<here|see https://metacpan.org/pod/Text::Fuzzy>.

=head1 SYNOPSIS

B<same.pl> C<some-directory>

=cut


my @file;
for my $dir (@ARGV) {
    #print "Doing directory: $dir\n";

    die "$0: cannot open $dir" 
        unless -X $dir;
    opendir my $DIR, $dir;

    while (my $object = readdir $DIR) {
        next unless -f $dir . "/" . $object;
        push @file, $dir . "/" . $object;
    }
    closedir $DIR;
}

for (my $i = 0; $i < @file - 1; $i ++ ) {
    my $tf = Text::Fuzzy->new ($file[$i]);
    for (my $j = $i; $j < @file; $j ++ ) {
        next if $i == $j;
        my $distance =$tf-> distance($file[$j]);
        my $metric = ($distance / 
            (length(basename($file[$i])) / 2 + 
             length(basename($file[$j])) / 2));
        next unless $metric <= 0.1;
        print 
            basename($file[$i]) . "," . 
            basename($file[$j]) . "," . 
            $metric . "," .
            $file[$i] . "," . 
            $file[$j] . "\n";
            
    }
}

=head1 AUTHOR

Written by Doug Salt.

=head1 REPORTING BUGS

Please send all bug reports to info@hutton.ac.uk.

=head1 COPYRIGHT

Copyright  ©  2020  The James Hutton Institute.  License GPLv3+: GNU
GPL version 3 or later L<http://gnu.org/licenses/gpl.html>.
This is free software: you are free  to  change  and  redistribute  it.
There is NO WARRANTY, to the extent permitted by law.

=head1 SEE ALSO

