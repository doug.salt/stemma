#!/usr/bin/env perl

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# Date March 2019

use strict;
use POSIX;

=encoding UTF-8

=head1 NAME

columnate - a small program to neatly columnate stuff for presentation. I wrote this
because the GNU column does not behave as expected with non-standard terminal
widths, and stubbornly refused to bend to my will.

=head1 SYNOPSIS

B<columnate.pl> C<some-text-file>

=cut

my $columns = 5;
open my $INPUT, "<" . $ARGV[0] 
    || die "Unable to open $ARGV[0]: $!";
my @line = <$INPUT>;
close $INPUT;
chomp(@line);

my $longest = 0;
for (@line) {
    $longest = length($_) if length($_) > $longest
}

my $nof_lines = ceil(@line / $columns);

for (my $i = 0; $i < $nof_lines; $i++) {
    for (my $j = 0; $j < $columns ; $j++ ) {
        my $word = $line[$nof_lines * $j + $i];
        if (defined $word) {
            print $word;
            if ( $j >= $columns - 1 ) {
                print "\n";
            } 
            else {
                print " " x ($longest - length($word))
            }
        }
    }
}

=head1 AUTHOR

Written by Doug Salt.

=head1 REPORTING BUGS

Please send all bug reports to info@hutton.ac.uk.

=head1 COPYRIGHT

Copyright  ©  2020  The James Hutton Institute.  License GPLv3+: GNU
GPL version 3 or later L<http://gnu.org/licenses/gpl.html>.
This is free software: you are free  to  change  and  redistribute  it.
There is NO WARRANTY, to the extent permitted by law.

=head1 SEE ALSO

