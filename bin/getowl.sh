#!/bin/sh

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# April 2019

# A small script to obtain the actual schema, underlying a specification.

# This only works if the correct redirect has been set in the specification
# document. For the PROV-0 I had to go digging through the page source for
# the specification (which was buried in the comments).

curl -L -H "accept: application/rdf+xml" "$1"

# Note: W3C have not applied for an owl mime-type yet!

# Hmmmm, this site seems quite interesting. https://dvcs.w3.org/hg
