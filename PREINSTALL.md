# WordNet

## Slackware

To install WordNet in slackware

```
slapt-src -i WordNet
```

The dictionary directory on Slackware is /usr/dict/


## Cygwin


WordNet is available in the Cygwin repositories.
The dictionary directory on Cygwin is /usr/lib/wnres/dict/

## Common

```
perl -MCPAN -e "install Lingua::EN::Inflexion"
wget http://search.cpan.org/CPAN/authors/id/D/DB/DBRIAN/Lingua-Wordnet-0.74.tar.gz
tar -zxvf Lingua-Wordnet-0.74.tar.gz
cd Lingua-Wordnet-0.74
perl Makefile.PL
make
make install
cd ~/git/stemma/wordnet
./convertdb.pl
```
Supply the dictionary directory (by default, on Slackware, this will be
/usr/dict) and then write the files to the directory in which you are running
the conversion.  The ~/git/SMARTEES/bin/wordnet.pl must be modified to reflect
this change (and the OS it is running on).

# R

## Slackware

To install R for running the scripts in this repository

```
slapt-get -i R
R
install.packages("optargs")
```
And answer all the defaults


