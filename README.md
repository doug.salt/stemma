# PURPOSE

![Smartees Logo](http://local-social-innovation.eu/fileadmin/templates/code/images/logos/logo.svg)

This contains the Perl code that can trawl documents for terminology. This are
high-frequency words with low 'familiarity'.

This is a repository that allows me to publish all the
[SMARTEES](https://local-social-innovation.eu/) terrminology extract stuff.

I've been experimenting with abbreviations, and depluralisation,

The main programs in the suite are some [Perl](www.perl.org) code to produce
csv files with n-grams against usage frequency and WordNet "familiarity".

The second is a simple [R](https://www.r-project.org/) script to take this
input and visualise it in a pretty manner.

I have rebuilt the graphs for familarity vs frequency. We had a good
correlation with 1-gram against familiarity, but not so the case with n-grams n
\in {2,3}, so rather than adding all the familiarities of the


# TODO

1. I am wondering if I can train a neural net to tidy up documents for me. I
   want to investigate this. This would be massively useful.
2. I also want to investigate Pattern from CLiPS
   (https://www.clips.uantwerpen.be/pages/pattern-web).
3. In addition I want to look at some of the other metrics in WordNet,
   particular with regards to de-pluralising.
4. Blend the R code into the Perl code, so there is just one program.

# USE

All this is written in Perl (and why not, as it is rather wonderfully expressive).
The main programs are:

1. bin/get_frequencies.pl
2. bin/plot_frequency_vs_familiarity.R


The first will take a pdf, word document or plain text and do its magic
counting frequencies of words against their "familiarity".  For more
information on the `get_frequencies.pl`, then 

```
perldoc bin/get_frequencies.pl
```

This produces an output csv file which is used as input to
`plot_frequency_vs_familiarity.R` For information on how to use 2. then please
consult the comments in the R code.

This code using [WordNet](https://wordnet.princeton.edu/), for more information
on how I used this then:

```
perldoc WordNet::QueryData
```

# MANIFEST

+ README.md - this file
+ bin - directory containing all the scripts.

# CONTACT 

doug.saltATThuttonDOTTacDOTTuk

